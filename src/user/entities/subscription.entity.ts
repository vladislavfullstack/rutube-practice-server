import { Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm'
import { Base } from '../../utils/base'
import { VideoEntity } from '../../video/entities/video.entity'
import { UserEntity } from './user.entity'

@Entity('SubscriptionEntity')
export class SubscriptionEntity extends Base {
	@ManyToOne(() => UserEntity, user => user.subscriptions)
	@JoinColumn({ name: 'from_user_id' })
	fromUser: UserEntity

	@OneToMany(() => VideoEntity, video => video.user)
	@JoinColumn({ name: 'to_channel_id' })
	toChannel: VideoEntity[]
}
